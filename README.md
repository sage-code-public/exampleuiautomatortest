# Example for Running UiAutomator Test


This project uses the Gradle build system. You don't need an IDE to build and execute it but Android Studio is recommended. I have included an example test which executes a simple script on the Google Chrome Browser in a running emulator. I tested this against a Pixel 2 emulator that was running Android 7.0 (with Google APIs). 

To setup the project and generate the apk:

1. Download the project code, preferably using `git clone`.
2. Open the Android SDK Manager (*Tools* Menu | *Android*) and make sure you have installed the *Android testing support library Repository* under *Extras*.
3. In Android Studio, select *File* | *Open...* and point to the `./build.gradle` file.
4. To generate the .apk, execute the gradle assembleDebugAndroidTest command in Android Studio or from the command line. You will find the apk file in the `app/build/outputs/apk/androidTest/debug/app-debug-androidTest.apk`

To run the test:

1. Install the apk on a running emulator, either by dragging the apk to the emaulator, or executing `adb install <path>/app-debug-androidTest.apk`.
2. Next ensure that you have `adb` installed on your machine and run the following command: `adb shell am instrument -w edu.sage.android.test/androidx.test.runner.AndroidJUnitRunner`
3. You can also run the test from Android Studio. You can also run specific test classes by following the instructions [here](https://developer.android.com/reference/android/support/test/runner/AndroidJUnitRunner.html?hl=es).

The application will be started on the device/emulator and a series of actions will be performed automatically.

If you are using Android Studio, the *Run* window will show the test results.

For additional documentation about how to write and run uiautomator tests, please see: https://developer.android.com/training/testing/ui-testing/uiautomator-testing 
